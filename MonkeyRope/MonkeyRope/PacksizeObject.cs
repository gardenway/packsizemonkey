﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonkeyRope
{
    public abstract class PackSizeObject : ILog, IProcessTempo
    {
        public void Log()
        {
            System.Console.WriteLine("Nothing in this object");
        }

        public abstract int ProcessTempo();
    }
}
