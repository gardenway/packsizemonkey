﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonkeyRope
{
    class Leopard : Animal
    {
        private const int LeopardMass = 30; // mass in kg

        public Leopard(Direction directionArg, Repository repositoryArg)
            : base(directionArg, repositoryArg, LeopardMass)
        {

        }
    }
}
