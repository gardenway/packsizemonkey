﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;

namespace MonkeyRope
{
    using static Repository;
    using static Repository.Locale;
    using static Repository.RepositoryType;

    public class GlobalRepository : PackSizeObject
    {
        // private members
        private BackgroundWorker backgroundWorker;   // backgroundWorker
        private static GlobalRepository _instance;
        List<Repository> repositories = new List<Repository>();
        private static Random processOrderRand;

        // Change this to 1 or 0 to cause all tests to run VERY fast.
        public int globalSleepTime { get; protected set; } = 100;

        // Singleton to get the global repository
        public static GlobalRepository GetInstance(View view)
        {
            if (_instance == null) {
                _instance = new GlobalRepository(view);
            }
            return _instance;
        }

        public static GlobalRepository GetInstance()
        {
            return _instance;
        }

        public void ReportProgress(Memento memento)
        {
            int repositoryIndex = (int)memento.mLocale;
            backgroundWorker.ReportProgress(0,memento);
        }

        public GlobalRepository(View view)
        {
            RepositoryFactory repositoryFactory = RepositoryFactory.GetInstance();
            backgroundWorker = new BackgroundWorker();  // Initialize backgroundWorker
            backgroundWorker.WorkerReportsProgress = true;
            backgroundWorker.DoWork +=
                new DoWorkEventHandler(backgroundWorker_DoWork); // set background event handler
            backgroundWorker.ProgressChanged +=
                new ProgressChangedEventHandler(view.ViewReportingCallback);

            // Create the two cliff pools (east and west) and the rope
            repositories.Add(repositoryFactory.Create(PoolType, West, this));
            repositories.Add(repositoryFactory.Create(PoolType, East, this));
            repositories.Add(repositoryFactory.Create(RopeType, Bridge, this));

            processOrderRand = new Random();
            StringRope();
        }

        // Reset to allow testing to be reentrant
        public void Reset()
        {
            _instance = null;
        }

        // This code is specific to the east/west pool and rope assumptions.
        // This would need to be modified if more Pools were added, more ropes were added.
        // Also, this code breaks abstraction by using "Pool" and "Rope" inside the Repository abstraction
        public void StringRope()
        {
            GetRope().StringRopeBetweenCliffs(GetEastPool(), GetWestPool());
            GetEastPool().ConnectRope(GetRope());
            GetWestPool().ConnectRope(GetRope());
        }

        public Repository GetRespositoryByDirection(Locale location)
        {
            return repositories[(int)location];
        }

        public Pool GetEastPool()
        {
            return (Pool)repositories[(int)East];
        }

        public Pool GetWestPool()
        {
            return (Pool)repositories[(int)West];
        }
        public Rope GetRope()
        {
            return (Rope)repositories[(int)Bridge];
        }

        public override int ProcessTempo()
        {
            return 0;
        }

        // Check to see if any of the repositories have work that can be done
        public bool IsThereWorkToDo()
        {
            foreach (Repository repository in repositories) {
                if (repository.Count() > 0)
                    return true;
            }
            return false;
        }

        public void AdjustDelay(int stepDelay)
        {
            globalSleepTime = stepDelay;
        }

        //public int BackgroundProcessLogicMethod(BackgroundWorker bw, int a)
            public int BackgroundProcessLogicMethod()
        {
            Arbiter arbiter;

            while (IsThereWorkToDo()) {
                arbiter = Arbiter.GetInstance();
                arbiter.ManageTraffic(GetWestPool(), GetEastPool());


                Pool eastPool = GetEastPool();
                Pool westPool = GetWestPool();
                Rope rope = GetRope();

                int randomNumber = processOrderRand.Next(repositories.Count());
                // randomNumber will be a number between 0 and 3.  We will round robin the respositories from
                // that see:


                // before randomizing the order, the right side always started first.  Now, it is
                // random.
                int count = repositories.Count();
                for (int i = 0; i < 3; i++) {
                    int repositoryIndex = (randomNumber + i) % count;
                    if (repositories[repositoryIndex].Count() > 0) {
                        repositories[repositoryIndex].ProcessTempo();
                        ReportProgress(repositories[repositoryIndex].CreateMemento());
                        Thread.Sleep(globalSleepTime);   // Sleep between steps
                    }
                }

            }
            return 0;
        }

        public void Start()
        {
            // Start BackgroundWorker
            try {
                backgroundWorker.RunWorkerAsync(2000);
            } catch(Exception ex) {
                // more than one thread called at the same time from start
            }
        }

        public void Stop()
        {
            // Stop BackgroundWorker
            if (!backgroundWorker.IsBusy)
                backgroundWorker.CancelAsync();
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {

            while (!backgroundWorker.CancellationPending) {
                BackgroundProcessLogicMethod();
                //backgroundWorker.ReportProgress(0);
                Thread.Sleep(globalSleepTime);   // Sleep between steps

            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            int managedThreadID = Thread.CurrentThread.ManagedThreadId;

            if (e.Cancelled)
                MessageBox.Show("Operation was canceled");
            else if (e.Error != null)
                MessageBox.Show(e.Error.Message);
            else
                MessageBox.Show(e.Result.ToString());
        }
    }
}
