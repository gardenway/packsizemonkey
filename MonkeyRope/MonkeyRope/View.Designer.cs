﻿namespace MonkeyRope
{
    partial class View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(View));
            this.buttonAddMonkeysToWestPool = new System.Windows.Forms.Button();
            this.textBoxWestAddCount = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxEastAddCount = new System.Windows.Forms.TextBox();
            this.buttonAddMonkeysToEastPool = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelWestCliff = new System.Windows.Forms.Label();
            this.labelEastCliff = new System.Windows.Forms.Label();
            this.labelRope = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.buttonStart = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelAnimalSlot0 = new System.Windows.Forms.Label();
            this.labelAnimalSlot1 = new System.Windows.Forms.Label();
            this.labelAnimalSlot2 = new System.Windows.Forms.Label();
            this.labelEastBacklog = new System.Windows.Forms.Label();
            this.labelWestBacklog = new System.Windows.Forms.Label();
            this.pictureBoxSlot2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxSlot1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxSlot0 = new System.Windows.Forms.PictureBox();
            this.westPictureBox = new System.Windows.Forms.PictureBox();
            this.eastPictureBox = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.textBoxSleepTime = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.buttonStop = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSlot2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSlot1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSlot0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.westPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eastPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonAddMonkeysToWestPool
            // 
            this.buttonAddMonkeysToWestPool.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddMonkeysToWestPool.Location = new System.Drawing.Point(12, 610);
            this.buttonAddMonkeysToWestPool.Name = "buttonAddMonkeysToWestPool";
            this.buttonAddMonkeysToWestPool.Size = new System.Drawing.Size(127, 46);
            this.buttonAddMonkeysToWestPool.TabIndex = 11;
            this.buttonAddMonkeysToWestPool.Text = "Add Monkeys To West Pool";
            this.buttonAddMonkeysToWestPool.UseVisualStyleBackColor = true;
            this.buttonAddMonkeysToWestPool.Click += new System.EventHandler(this.buttonAddMonkeysToPool_Click);
            // 
            // textBoxWestAddCount
            // 
            this.textBoxWestAddCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxWestAddCount.Location = new System.Drawing.Point(42, 675);
            this.textBoxWestAddCount.Name = "textBoxWestAddCount";
            this.textBoxWestAddCount.Size = new System.Drawing.Size(45, 26);
            this.textBoxWestAddCount.TabIndex = 12;
            this.textBoxWestAddCount.Text = "10";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 659);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Number of Monkeys to Add:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(969, 670);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(139, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Number of Monkeys to Add:";
            // 
            // textBoxEastAddCount
            // 
            this.textBoxEastAddCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEastAddCount.Location = new System.Drawing.Point(1007, 686);
            this.textBoxEastAddCount.Name = "textBoxEastAddCount";
            this.textBoxEastAddCount.Size = new System.Drawing.Size(45, 26);
            this.textBoxEastAddCount.TabIndex = 15;
            this.textBoxEastAddCount.Text = "10";
            // 
            // buttonAddMonkeysToEastPool
            // 
            this.buttonAddMonkeysToEastPool.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddMonkeysToEastPool.Location = new System.Drawing.Point(969, 614);
            this.buttonAddMonkeysToEastPool.Name = "buttonAddMonkeysToEastPool";
            this.buttonAddMonkeysToEastPool.Size = new System.Drawing.Size(127, 46);
            this.buttonAddMonkeysToEastPool.TabIndex = 14;
            this.buttonAddMonkeysToEastPool.Text = "Add Monkeys To East Pool";
            this.buttonAddMonkeysToEastPool.UseVisualStyleBackColor = true;
            this.buttonAddMonkeysToEastPool.Click += new System.EventHandler(this.buttonAddMonkeysToPool_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 721);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(227, 26);
            this.label3.TabIndex = 17;
            this.label3.Text = "West Cliff Pool Report";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(916, 721);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(161, 20);
            this.label4.TabIndex = 26;
            this.label4.Text = "East Cliff Pool Report";
            // 
            // labelWestCliff
            // 
            this.labelWestCliff.AutoSize = true;
            this.labelWestCliff.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWestCliff.Location = new System.Drawing.Point(18, 747);
            this.labelWestCliff.Name = "labelWestCliff";
            this.labelWestCliff.Size = new System.Drawing.Size(69, 20);
            this.labelWestCliff.TabIndex = 19;
            this.labelWestCliff.Text = "Count: 0";
            // 
            // labelEastCliff
            // 
            this.labelEastCliff.AutoSize = true;
            this.labelEastCliff.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEastCliff.Location = new System.Drawing.Point(851, 747);
            this.labelEastCliff.Name = "labelEastCliff";
            this.labelEastCliff.Size = new System.Drawing.Size(65, 20);
            this.labelEastCliff.TabIndex = 20;
            this.labelEastCliff.Text = "Count 0";
            // 
            // labelRope
            // 
            this.labelRope.AutoSize = true;
            this.labelRope.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRope.Location = new System.Drawing.Point(435, 747);
            this.labelRope.Name = "labelRope";
            this.labelRope.Size = new System.Drawing.Size(48, 20);
            this.labelRope.TabIndex = 21;
            this.labelRope.Text = "Rope";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(234, 266);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 20);
            this.label5.TabIndex = 22;
            this.label5.Text = "Slot 0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(541, 266);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 20);
            this.label6.TabIndex = 23;
            this.label6.Text = "Slot 1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(821, 266);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 20);
            this.label7.TabIndex = 24;
            this.label7.Text = "Slot 2";
            // 
            // buttonStart
            // 
            this.buttonStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStart.Location = new System.Drawing.Point(529, 596);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(109, 65);
            this.buttonStart.TabIndex = 25;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(455, 717);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(135, 26);
            this.label8.TabIndex = 26;
            this.label8.Text = "Rope Report";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(194, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(740, 26);
            this.label9.TabIndex = 27;
            this.label9.Text = "First, Click on Buttons to Add Monkeys to Both Sides...Then Click on \"Start\"";
            // 
            // labelAnimalSlot0
            // 
            this.labelAnimalSlot0.AutoSize = true;
            this.labelAnimalSlot0.Location = new System.Drawing.Point(239, 68);
            this.labelAnimalSlot0.Name = "labelAnimalSlot0";
            this.labelAnimalSlot0.Size = new System.Drawing.Size(62, 13);
            this.labelAnimalSlot0.TabIndex = 28;
            this.labelAnimalSlot0.Text = "MonkeyID0";
            // 
            // labelAnimalSlot1
            // 
            this.labelAnimalSlot1.AutoSize = true;
            this.labelAnimalSlot1.Location = new System.Drawing.Point(543, 68);
            this.labelAnimalSlot1.Name = "labelAnimalSlot1";
            this.labelAnimalSlot1.Size = new System.Drawing.Size(62, 13);
            this.labelAnimalSlot1.TabIndex = 28;
            this.labelAnimalSlot1.Text = "MonkeyID1";
            // 
            // labelAnimalSlot2
            // 
            this.labelAnimalSlot2.AutoSize = true;
            this.labelAnimalSlot2.Location = new System.Drawing.Point(830, 68);
            this.labelAnimalSlot2.Name = "labelAnimalSlot2";
            this.labelAnimalSlot2.Size = new System.Drawing.Size(62, 13);
            this.labelAnimalSlot2.TabIndex = 28;
            this.labelAnimalSlot2.Text = "MonkeyID2";
            // 
            // labelEastBacklog
            // 
            this.labelEastBacklog.AutoSize = true;
            this.labelEastBacklog.BackColor = System.Drawing.Color.GreenYellow;
            this.labelEastBacklog.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEastBacklog.ForeColor = System.Drawing.Color.Red;
            this.labelEastBacklog.Location = new System.Drawing.Point(1002, 101);
            this.labelEastBacklog.Name = "labelEastBacklog";
            this.labelEastBacklog.Size = new System.Drawing.Size(75, 25);
            this.labelEastBacklog.TabIndex = 29;
            this.labelEastBacklog.Text = "label10";
            // 
            // labelWestBacklog
            // 
            this.labelWestBacklog.AutoSize = true;
            this.labelWestBacklog.BackColor = System.Drawing.Color.GreenYellow;
            this.labelWestBacklog.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWestBacklog.ForeColor = System.Drawing.Color.Red;
            this.labelWestBacklog.Location = new System.Drawing.Point(18, 101);
            this.labelWestBacklog.Name = "labelWestBacklog";
            this.labelWestBacklog.Size = new System.Drawing.Size(75, 25);
            this.labelWestBacklog.TabIndex = 29;
            this.labelWestBacklog.Text = "label10";
            // 
            // pictureBoxSlot2
            // 
            this.pictureBoxSlot2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxSlot2.Image")));
            this.pictureBoxSlot2.Location = new System.Drawing.Point(799, 84);
            this.pictureBoxSlot2.Name = "pictureBoxSlot2";
            this.pictureBoxSlot2.Size = new System.Drawing.Size(102, 133);
            this.pictureBoxSlot2.TabIndex = 8;
            this.pictureBoxSlot2.TabStop = false;
            // 
            // pictureBoxSlot1
            // 
            this.pictureBoxSlot1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxSlot1.Image")));
            this.pictureBoxSlot1.Location = new System.Drawing.Point(521, 84);
            this.pictureBoxSlot1.Name = "pictureBoxSlot1";
            this.pictureBoxSlot1.Size = new System.Drawing.Size(102, 133);
            this.pictureBoxSlot1.TabIndex = 7;
            this.pictureBoxSlot1.TabStop = false;
            // 
            // pictureBoxSlot0
            // 
            this.pictureBoxSlot0.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxSlot0.Image")));
            this.pictureBoxSlot0.Location = new System.Drawing.Point(210, 84);
            this.pictureBoxSlot0.Name = "pictureBoxSlot0";
            this.pictureBoxSlot0.Size = new System.Drawing.Size(102, 133);
            this.pictureBoxSlot0.TabIndex = 6;
            this.pictureBoxSlot0.TabStop = false;
            // 
            // westPictureBox
            // 
            this.westPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("westPictureBox.Image")));
            this.westPictureBox.Location = new System.Drawing.Point(1, 152);
            this.westPictureBox.Name = "westPictureBox";
            this.westPictureBox.Size = new System.Drawing.Size(101, 120);
            this.westPictureBox.TabIndex = 4;
            this.westPictureBox.TabStop = false;
            // 
            // eastPictureBox
            // 
            this.eastPictureBox.InitialImage = global::MonkeyRope.Properties.Resources.WestwardMore;
            this.eastPictureBox.Location = new System.Drawing.Point(1033, 152);
            this.eastPictureBox.Name = "eastPictureBox";
            this.eastPictureBox.Size = new System.Drawing.Size(102, 117);
            this.eastPictureBox.TabIndex = 0;
            this.eastPictureBox.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::MonkeyRope.Properties.Resources.FullCliff;
            this.pictureBox5.Location = new System.Drawing.Point(1, 152);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(1134, 418);
            this.pictureBox5.TabIndex = 9;
            this.pictureBox5.TabStop = false;
            // 
            // textBoxSleepTime
            // 
            this.textBoxSleepTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSleepTime.Location = new System.Drawing.Point(560, 1009);
            this.textBoxSleepTime.Name = "textBoxSleepTime";
            this.textBoxSleepTime.Size = new System.Drawing.Size(45, 26);
            this.textBoxSleepTime.TabIndex = 12;
            this.textBoxSleepTime.Text = "10";
            this.textBoxSleepTime.TextChanged += new System.EventHandler(this.textBoxSleepTime_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(1063, 1291);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(45, 26);
            this.textBox2.TabIndex = 12;
            this.textBox2.Text = "10";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(511, 993);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(127, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "Step Delay in miliseconds";
            // 
            // buttonStop
            // 
            this.buttonStop.Enabled = false;
            this.buttonStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStop.Location = new System.Drawing.Point(999, 941);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(109, 65);
            this.buttonStop.TabIndex = 30;
            this.buttonStop.Text = "Stop";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Visible = false;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.ClientSize = new System.Drawing.Size(1135, 1047);
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.labelWestBacklog);
            this.Controls.Add(this.labelEastBacklog);
            this.Controls.Add(this.labelAnimalSlot2);
            this.Controls.Add(this.labelAnimalSlot1);
            this.Controls.Add(this.labelAnimalSlot0);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.labelRope);
            this.Controls.Add(this.labelEastCliff);
            this.Controls.Add(this.labelWestCliff);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxEastAddCount);
            this.Controls.Add(this.buttonAddMonkeysToEastPool);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBoxSleepTime);
            this.Controls.Add(this.textBoxWestAddCount);
            this.Controls.Add(this.buttonAddMonkeysToWestPool);
            this.Controls.Add(this.pictureBoxSlot2);
            this.Controls.Add(this.pictureBoxSlot1);
            this.Controls.Add(this.pictureBoxSlot0);
            this.Controls.Add(this.westPictureBox);
            this.Controls.Add(this.eastPictureBox);
            this.Controls.Add(this.pictureBox5);
            this.Name = "View";
            this.Text = "Monkey Rope Simulation";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSlot2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSlot1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSlot0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.westPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eastPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox eastPictureBox;
        private System.Windows.Forms.PictureBox westPictureBox;
        private System.Windows.Forms.PictureBox pictureBoxSlot0;
        private System.Windows.Forms.PictureBox pictureBoxSlot1;
        private System.Windows.Forms.PictureBox pictureBoxSlot2;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Button buttonAddMonkeysToWestPool;
        private System.Windows.Forms.TextBox textBoxWestAddCount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxEastAddCount;
        private System.Windows.Forms.Button buttonAddMonkeysToEastPool;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelWestCliff;
        private System.Windows.Forms.Label labelEastCliff;
        private System.Windows.Forms.Label labelRope;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelAnimalSlot0;
        private System.Windows.Forms.Label labelAnimalSlot1;
        private System.Windows.Forms.Label labelAnimalSlot2;
        private System.Windows.Forms.Label labelEastBacklog;
        private System.Windows.Forms.Label labelWestBacklog;
        private System.Windows.Forms.TextBox textBoxSleepTime;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button buttonStop;
    }
}

