﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;
using System.ComponentModel;

namespace MonkeyRope
{
    using static Repository;
    using static Repository.Locale;
    using static Animal;

    public partial class View : Form
    {

        private static List<PictureBox> bridgePictures = new List<PictureBox>();
        private static List<Label> bridgeLabels = new List<Label>();
        private static Dictionary<Locale, Label> backlogLabels =
            new Dictionary<Locale, Label>();
        private static Dictionary<Locale, PictureBox> cliffPictures =
            new Dictionary<Locale, PictureBox>();
        private static Dictionary<Locale, Label> labels =
                    new Dictionary<Locale, Label>();
        private static System.Drawing.Image[,] chimpImages =
        {{Properties.Resources.WestwardSingle,Properties.Resources.EaswardSingle },
         {Properties.Resources.sDoubleChimps, Properties.Resources.sDoubleChimps },
         {Properties.Resources.WestwardMore, Properties.Resources.EastwardMore }
        };

        enum PluralityRowIndex { Single = 0, Two = 1, More = 2 };
        public delegate void DelegateCallback(Memento memento);
        private GlobalRepository globalRespository = null;

        public View()
        {
            InitializeComponent();

            // Saving static pointers to our labels for static callbacks
            labels[West] = labelWestCliff;
            labels[East] = labelEastCliff;
            labels[Bridge] = labelRope;

            // Initilize the list of pictures to hang over each slot on the bridge
            // It would be nice to refactor this to more closely couple these with 
            // the Rope class, but WindowsForms really does not lend itself to a 
            // strict MVC model.  Laravel and Rails frameworks are much better in that 
            // respect.
            bridgePictures.Add(pictureBoxSlot0);
            bridgePictures.Add(pictureBoxSlot1);
            bridgePictures.Add(pictureBoxSlot2);
            bridgeLabels.Add(labelAnimalSlot0);
            bridgeLabels.Add(labelAnimalSlot1);
            bridgeLabels.Add(labelAnimalSlot2);

            backlogLabels[West] = labelWestBacklog;
            backlogLabels[East] = labelEastBacklog;

            cliffPictures[West] = westPictureBox;
            cliffPictures[East] = eastPictureBox;

            globalRespository = GlobalRepository.GetInstance(this);  // Use singleton to get and initialize the global repository
            Arbiter.GetInstance();  // initialize arbiter
            textBoxSleepTime.Text = globalRespository.globalSleepTime.ToString();
            int managedThreadID = Thread.CurrentThread.ManagedThreadId;
            //labelThreadID.Text = managedThreadID.ToString();

        }


        public void ViewReportingCallback(object sender, ProgressChangedEventArgs e)
        {
            //labelThreadID.Text = (e.ProgressPercentage.ToString() + "%");

            
            if (e.UserState != null) {
                Memento memento = (Memento)e.UserState;

                switch (memento.mLocale) {
                    case Bridge:
                        ReportRopeCallback(memento);
                        break;
                    case West:
                        ReportPoolCallback(memento);
                        break;
                    case East:
                        ReportPoolCallback(memento);
                        break;
                }
            }
        }
        /* 
         * callback function to let the view know what the count of the east pool is
         * At first we had three callback functions.  To simply this, we use the
        *  Memento pattern to pass the entire state of the repository (rope, or cliff pool) to
        *  be avaiable to the view.
        *
        *  Adding the memento pattern reduced the number of callbacks, made the startup code simpler.
        *  More importatly, it made future changes to the reporting funtion much easier and more extensible.
        * 
        * Finally decided to do this to make it easier to keep track of the gates and to track the efficiency of the arbiter.
        * 
        */
        private void ReportPoolCallback(Memento memento)
        {
            Locale locale = memento.mLocale;
            PluralityRowIndex chimpRoxIndex;

            labels[locale].Text = "Current # waiting: " + memento.mCount.ToString() + "\n" +
                "GateDirection: " + Arbiter.GetInstance().gateDirection.ToString() + "\n" +
                "waitCount: " + memento.mWaitCount.ToString() + "\n" +
                "turnCount: " + memento.mTurnCount.ToString() + "\n" +
                "maxWaitCount: " + memento.mWaitCountMax.ToString() + "\n" +
                "AnimalsProcessedEastBound: " + memento.mAnimalsProcessedEastbound + "\n" +
                "AnimalsProcessedWestBound: " + memento.mAnimalsProcessedWestbound + "\n" +
                "AnimalsProcessed: " + memento.mAnimalsProcessed;

            backlogLabels[locale].Text = memento.mCount.ToString();

            if (memento.mCount == 0) {
                cliffPictures[locale].Hide();
            } else {
                // Get the of the image table.
                // 0th row is if there is one chimp: Plurality: 0
                // 1st row is if there are 2 chimps: Plurality: 1
                // 2nd row is if there are more than 2 chimps: Plurality: 2
                // Since the table is 0 based and the chimp count is 1-based,
                // we have to subtract 1 before determing the index
                chimpRoxIndex = (PluralityRowIndex)(memento.mCount - 1);
                chimpRoxIndex = chimpRoxIndex >= PluralityRowIndex.More ? PluralityRowIndex.More : chimpRoxIndex;
                cliffPictures[locale].Image = chimpImages[(int)chimpRoxIndex, (int)locale];
                cliffPictures[locale].Show();
            }
        }

        // Callback to get the state of the Rope repository
        // To refactor more, we might want to prevent the callback from returnin an Animal array.  
        // However, right now the animal gives us 3 things (
        //  1. whether each slot is empty
        //  2. the direction that each animal is traveling which is used by the view form to display the 
        //              correct orientation of the monkey
        //  3. the id number of the animal in each slot
        public void ReportRopeCallback(Memento memento)
        {
            string slotReport = "";

            int i = 0;
            foreach (SlotMomento slotMomento in memento.mSlots) {
                if (slotMomento.animalId == 0) {  // slot is empty
                    bridgePictures[i].Hide();
                    bridgeLabels[i].Hide();
                } else {

                    int managedThreadID = Thread.CurrentThread.ManagedThreadId;


                    bridgePictures[i].Image =
                        chimpImages[(int)PluralityRowIndex.Single, (int)slotMomento.direction];
                    bridgePictures[i].Show();

                    bridgeLabels[i].Text =
                        slotMomento.animalId.ToString() + ", " + slotMomento.name;
                    bridgeLabels[i].Show();

                }

                slotReport += "Slot" + i++ + ": " +
                    (slotMomento.animalId == 0 ?
                        "empty" :
                       "#" + slotMomento.animalId.ToString()) + ", " +
                       slotMomento.name +
                       "\n";

            }

            slotReport += "AnimalsProcessedEastBound: " + memento.mAnimalsProcessedEastbound + "\n" +
                 "AnimalsProcessedWestBound: " + memento.mAnimalsProcessedWestbound + "\n" +
                "AnimalsProcessed: " + memento.mAnimalsProcessed;

            labels[Locale.Bridge].Text = slotReport;
            
        }

        private void buttonAddMonkeysToPool_Click(object sender, EventArgs e)
        {


            AnimalFactory animalFactory = AnimalFactory.GetInstance();

            int addCount = 0;
            // Use TryParse to catch the exception from a text input that does not parse to an integer.
            // TryParse will set addCount to 0 if the parse fails.  We are not putting up a message
            // for a failed parse.  That will be let for another day.


            var button = (Button)sender;
            Pool pool = null;
            switch (button.Name) {
                case "buttonAddMonkeysToEastPool":
                    pool = (Pool)GlobalRepository.GetInstance(this).GetRespositoryByDirection(East);
                    Int32.TryParse(textBoxEastAddCount.Text, out addCount);
                    break;
                case "buttonAddMonkeysToWestPool":
                    pool = (Pool)GlobalRepository.GetInstance(this).GetRespositoryByDirection(West);
                    Int32.TryParse(textBoxWestAddCount.Text, out addCount);
                    break;
            }

            if (pool != null) {
                for (int i = 0; i < addCount; i++) {
                    pool.AddAnimal(AnimalType.Monkey);
                }
            }
        }



        private void textBoxSleepTime_TextChanged(object sender, EventArgs e)
        {
            int stepDelay;
            if (Int32.TryParse(textBoxSleepTime.Text, out stepDelay)) {
                globalRespository.AdjustDelay(stepDelay);
            }
        }

        // background thread buttons
        private void buttonStart_Click(object sender, EventArgs e)
        {
            globalRespository.Start();
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            globalRespository.Stop();

        }
    }
}
