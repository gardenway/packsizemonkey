﻿
namespace MonkeyRope
{
    using static Animal;

    public class Arbiter : ILog
    {
        private static Arbiter _instance;
        private int maxAllowedTurnCount = 5;
        public Direction gateDirection { get; protected set; } = Direction.None;

        public static Arbiter GetInstance()
        {
            if (_instance == null) {
                _instance = new Arbiter();
            }
            return _instance;
        }

        /// <summary>
        /// Algorithm for allowing orderly flow:
        /// 
        /// 2 variables provide the metrics for modifying the direction of the one-way gate:
        ///     1. west.turnCount
        ///     2. east.turnCount
        ///     
        /// turnCount counts the number of animals that get onto the rope while 
        ///     one side is not able to get on the rope.  turnCount is incremented
        ///     at Pool->MoveToRope(), each time one side gets to put a monkey on the 
        ///     rope.  It does not matter wether the rope is "blocked", because every
        ///     time a monkey gets started on the rope, the rope is blocked.
        ///      
        ///     turnCount is reset any time that gate is switched
        ///     
        /// The gate is switched any time one side has had their max number of turns
        ///     at the same time that the other side has work to do.
        ///     
        /// </summary>
        /// <param name="westPool"></param>
        /// <param name="eastPool"></param>
        public void ManageTraffic(Pool westPool, Pool eastPool)
        {
            Memento westMemento, eastMemento;

            westMemento = westPool.CreateMemento();
            eastMemento = eastPool.CreateMemento();
            
            if (gateDirection == Direction.None) {
                // We are going to set the direction to what it currently is,
                // and then we will switch it.
                if (eastMemento.mTurnCount >
                    westMemento.mTurnCount) {
                    // We started off going westward, set it the other way:
                    gateDirection = Direction.Westward;
                } else {
                    // We started off going eastware, set it the other way:
                    gateDirection = Direction.Eastward;
                }
            }

            // Open our gate if the other side has had max turns and we have stuff to do
            switch (gateDirection) {
                case Direction.Eastward:
                    if (westMemento.mTurnCount > maxAllowedTurnCount ||
                        westMemento.mCount == 0)
                        SwitchGate(westPool, eastPool, westMemento, eastMemento);
                    break;
                case Direction.Westward:
                    if (eastMemento.mTurnCount > maxAllowedTurnCount ||
                        eastMemento.mCount == 0)
                        SwitchGate(westPool, eastPool, westMemento, eastMemento);
                    break;
                case Direction.None:

                    break;
            }
        }

        private void SwitchGate(Pool westPool, Pool eastPool,
            Memento westMemento, Memento eastMemento)
        {

            switch (gateDirection) {
                case Direction.Eastward:
                    // Make it go Westward
                    gateDirection = Direction.Westward;
                    break;
                case Direction.Westward:
                    // Make it go Eastward
                    gateDirection = Direction.Eastward;
                    break;
            }

            westPool.GateSwitched();  // Cause pools to reset their metrics
            eastPool.GateSwitched();  // Cause pools to reset their metrics
        }
        public void Log() { }  // not yet implemented
    }
}
