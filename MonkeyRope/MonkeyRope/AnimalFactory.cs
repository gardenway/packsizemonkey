﻿using System;

namespace MonkeyRope
{
    using static Animal;

    public class AnimalFactory : ILog
    {
        public void Log() { }


        private static AnimalFactory _instance = null; // singleton pointer for AnimalFactory

        protected AnimalFactory() { }

        public static AnimalFactory GetInstance()
        {
            if(_instance == null) {
                _instance = new AnimalFactory();
            }
            return _instance;
        }

        public Animal Create(AnimalType animalTypeArg, Animal.Direction direction, 
            Repository repository)
        {
            Animal newAnimal = null;
            var currentNamespace = System.Reflection.Assembly.
                    GetExecutingAssembly().GetName().Name;
            string className = currentNamespace + "." + animalTypeArg.ToString();
            // We want to be able to pass the parameters to the CreateInstance call,
            // so we need to create an object array of the parameters.
            // Amazing what you can do with C#!
            newAnimal = (Animal)GetInstance(className,
                new object[] { direction, repository });
            return newAnimal;
        }

        //  GetInstance class to create an object from a string name of the object.
        // See Sarath Avanavu's answer at
        // https://stackoverflow.com/questions/223952/create-an-instance-of-a-class-from-a-string
        // Also, Sarath's answer includes code to search all loaded assemblies for the object type, 
        // but we do not need the extra for loop.  Modified Sarath's code to pass the constructor
        // parameters.
        public object GetInstance(string strFullyQualifiedName, 
            object[] objectArray = null)
        {
            Type type = Type.GetType(strFullyQualifiedName);
            if (objectArray == null) {
                return Activator.CreateInstance(type);
            } else {
                return Activator.CreateInstance(type, objectArray);
            }
        }

    }
}
