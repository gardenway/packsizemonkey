﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MonkeyRope
{
    public abstract class Repository : ILog, IMove
    {
        public int turnCount { get; protected set; } = 0;
        public int animalsProcessed { get; protected set; } = 0;
        public int animalsProcessedEastbound { get; protected set; } = 0;
        public int animalsProcessedWestbound { get; protected set; } = 0;
        protected GlobalRepository globalRepository;
        public enum RepositoryType { PoolType, RopeType };

        protected Queue<Animal> queue = new Queue<Animal>();

        // assign integers to the Location enum so that 
        // these can be used as indices in an array/list
        public enum Locale { West = 0, East = 1, Bridge = 2 };

        public Locale locale
        {
            get;
            protected set;
        }
        
        // abstract methods that must be implemented in Rope and Pool
        // haven't gotten around to writing the Logging code yet.
        public abstract void Log();
        public abstract Memento CreateMemento();
        public abstract int ProcessTempo();
        
        // I do not like the way the animals are handed off between the cliff and the
        // rope.  This needs to be rewritten.
        public abstract bool TryMoveFromRepository(Animal animal);

        // this is used in a couple of places 1) sent back to the callbacks for the display of the 
        // monkeys on the cliff, and 2) to determine if the work is complete.
        public abstract int Count();

    }
}
