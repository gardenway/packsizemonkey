﻿using System;
using System.Linq;


namespace MonkeyRope
{
    using static Animal;
    using static Animal.Direction;


    public class Pool : Repository, IMove, ILog
    {
        // Protected and Private Members and protected set members
        private Rope rope;
        
        public int waitCount { get; protected set; } = 0;// the number of cylces that the backlog has been present without opening the gate
        public int waitCountMax { get; protected set; } = 0;

        protected View.DelegateCallback callback;

        // constructor
        public Pool(Locale localeArg, GlobalRepository globalRepositoryArg)
        {
            globalRepository = globalRepositoryArg;
            locale = localeArg;
            globalRepository.ReportProgress(CreateMemento());
        }

        // save the current state in a memento object and return it
        public override Memento CreateMemento()
        {
            Memento memento = new Memento
            {
                mLocale = locale,
                
                mCount = Count(),
                mSlots = null,
                mTurnCount = turnCount,
                mWaitCount = waitCount,
                mWaitCountMax = waitCountMax,
                mAnimalsProcessed = animalsProcessed,
                mAnimalsProcessedEastbound = animalsProcessedEastbound,
                mAnimalsProcessedWestbound = animalsProcessedWestbound
            };
            return memento;
        }

        public override void Log() { }

        public override bool TryMoveFromRepository(Animal animal)
        {
            return false;
        }

        private bool IsGateOpenForUs()
        {
            switch (locale) {
                case Locale.East:
                    if(Arbiter.GetInstance().gateDirection == Direction.Eastward) {
                        return false;
                    }
                    break;
                case Locale.West:
                    if (Arbiter.GetInstance().gateDirection == Direction.Westward) {
                        return false;
                    }
                    break;
            }
            return true;
        }

        // Rope is blocked if the gate is the wrong way or 
        //    the rope is flowing in the wrong way
        public bool IsRopeBlocked()
        {
            if (IsGateOpenForUs() && IsRopeDirectionOK()) {
                waitCount = 0;
                return false;
            } else {
                waitCount++;
                waitCountMax = Math.Max(waitCount, waitCountMax);
                return true;
            }
        }
        public bool IsRopeDirectionOK()
        {
            Direction ropeDirection = rope.MonkeysDirectionOnRope();
            switch (ropeDirection) {
                case Eastward:
                    if (locale == Locale.West)
                        return true;
                    else
                        return false;

                case Westward:
                    if (locale == Locale.East)
                        return true;
                    else
                        return false;

                case None:
                    return true;
            }
            return false;
        }

        public bool IsRopeFirstSlotOpen()
        {
            return rope.IsRopeFirstSlotOpen(locale);
        }

        public void MoveToRope(Animal animal)
        {
            animalsProcessed++;
            turnCount++;
            if (locale == Locale.East)
                animalsProcessedWestbound++;
            else if (locale == Locale.West)
                animalsProcessedEastbound++;

            rope.MoveToRope(animal, locale);
            globalRepository.ReportProgress(CreateMemento());
        }
        
        public void GateSwitched()
        {
            
            waitCountMax = Math.Max(waitCountMax, waitCount);
            turnCount = 0;
            waitCount = 0;
        }
        public void AddAnimal(AnimalType animalType)
        {
            AnimalFactory animalFactory = AnimalFactory.GetInstance();

            // So, I want my animals to have internal desires, like objects :-)  
            //  It is the animal that wants to go east or west, not the location where they are.  this is important once they get on the bridge.
            // When they are on the bridge, we would have animals that decide to reverse directions unless it is clear which way they want to go.
            // We could have just had all animals on bridges move to the next open slot, but then what happens if you have a single animal on the bridge?
            // The single animal in the middle of the bridge should have a direction, otherwise, it might switch directions, or worse, the bridge would have
            // to communicate to the animal that this is a one way bridge until the direction of flow changes.
            // So, all animals upon creation must have a desired direction of travel (east or west).
            //
            // With that being said, how do we decide which animals will be eastward leaning?  For now, we are doing it based on which pool they are 
            // born into.  All animals created in the west pool are eastward leaning and all animals created in the east pool are westward leaning.
            // We could randomly assign leanings when an animal is created, but then we would end up with a bunch of westward animals in the 
            // west pool who would be happy to stay where they are.  We could write an algorithm to handle this and a timer that would cause them to randomly
            // change their desired direction, but that is for another day.
            //
            // for now, we will just create westward leaning animals in the east pool and vice versa.

            Animal animal =
                animalFactory.Create(animalType,
                (locale == Locale.East ? Animal.Direction.Westward : Animal.Direction.Eastward),
                this
                );  // If this is an East pool, then create a westward animal, or vice versa
            queue.Enqueue(animal);
            globalRepository.ReportProgress(CreateMemento());
        }

        public void ConnectRope(Rope ropeArg)
        {
            rope = ropeArg;
        }

        public void Remove()
        {
            queue.Dequeue(); // Should be this animal
            globalRepository.ReportProgress(CreateMemento());

        }
        public override int ProcessTempo()
        {
            try {
                if (queue.Count() > 0) {
                    Animal animal = queue.First();
                    animal.ProcessTempo();
                }
            } catch (Exception ex) {

            }
             globalRepository.ReportProgress(CreateMemento());

            return 0;
            }

        public override int Count()
        {
            return queue.Count();
        }

        public bool IsAtFrontOfQueue(Animal animal)
        {
            return (animal == queue.First());
        }
    }
}
