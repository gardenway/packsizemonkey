﻿using System.Collections.Generic;
using System.Linq;

namespace MonkeyRope
{
    using static Animal;
    using static Pool.Locale;

    public class Rope : Repository, ILog, IMove
    {
        // trying to think about how we can have more slots on a rope 
        //  in case we have to extend this problem in the future or if 
        // we want to improve the animation of the monkeys walking across the bridge

        private const int MonkeySlots = 3;
        private int ropeSlots = MonkeySlots;
        private Dictionary<Locale, Pool> cliffs = new Dictionary<Locale, Pool>();

        // Using an array for the slots because I want it to 
        // be a fixed length
        private Animal[] slots;

        //constructor
        public Rope(GlobalRepository globalRepositoryArg)
        {
            globalRepository = globalRepositoryArg;
            slots = InitializeRope();
            locale = Bridge;  // this is really telling us the type of Repository (east, west or bridge; bridge is a rope)
            globalRepository.ReportProgress(CreateMemento());
        }

        private Animal[] InitializeRope()
        {
            Animal[] returnSlots = new Animal[ropeSlots];

            for (int i = 0; i < ropeSlots; i++)
                returnSlots[i] = null;  // initialize all slots to empty
            return returnSlots;

        }
        public override void Log() { }  // not yet implemented

        /* 
         * Initialization methods
         * */

        public void StringRopeBetweenCliffs(Pool westPool, Pool eastPool)
        {
            cliffs.Add(West, westPool);
            cliffs.Add(East, eastPool);
        }

        /*
         * Move methods
         */

        public override bool TryMoveFromRepository(Animal animal)
        {
            if (MonkeysDirectionOnRope() == Direction.None)
                return true;
            if (MonkeysDirectionOnRope() == animal.direction)
                if (animal.direction == Direction.Eastward &&
                    WestSlotEmpty())
                    return true;
            if (MonkeysDirectionOnRope() == animal.direction)
                if (animal.direction == Direction.Westward &&
                    EastSlotEmpty())
                    return true;
            return true;
        }

        /*
         * State methods
         */
        public Animal.Position GetPosition(int animalId)
        {
            int slot = GetSlotByAnimalId(animalId);
            if (slot == -1) {
                return Position.InNirvana;
            } else {
                return Position.OnRope;
            }
        }

        // save the current state in a memento object and return it
        public override Memento CreateMemento()
        {
            Memento memento = new Memento
            {
                mLocale = locale,

                mCount = Count(),  // total animals on therope
                mSlots = ConvertSlotsToList(), // list of integers reflecting slots
                mWaitCount = 0,    // waitCount describes when someone wants to leave a repository, so this is always 0 for ropes
                mWaitCountMax = 0,  // same ax mWaitCount
                mTurnCount = 0,
                mAnimalsProcessed = animalsProcessed,
                mAnimalsProcessedEastbound = animalsProcessedEastbound,
                mAnimalsProcessedWestbound = animalsProcessedWestbound
            };
            return memento;
        }

        // add up the number of animals in the 3 slots of the rope and return the count
        public override int Count()
        {
            int count = 0;

            foreach (Animal animal in slots) {
                if (animal != null) {
                    count++;
                }
            }
            return count;
        }

        // for the callback funciton, we return a list of animalIds.  The 0 animal id is a null animal.
        public List<SlotMomento> ConvertSlotsToList()
        {
            List<SlotMomento> slotMomentos = new List<SlotMomento>();

            foreach (Animal animal in slots) {
                SlotMomento slotMomento = new SlotMomento();

                if (animal == null) {
                    slotMomento.animalId = 0;
                    slotMomentos.Add(slotMomento);
                } else {
                    slotMomento.animalId = animal.animalId;
                    slotMomento.direction = animal.direction;
                    slotMomento.name = animal.name;
                    slotMomentos.Add(slotMomento);
                }
            }
            return slotMomentos;
        }

        // return the slot number if animalId is found or -1 if not found
        private int GetSlotByAnimalId(int animalId)
        {
            for (int i = 0; i < slots.Count(); i++) {
                if (slots[i].animalId == animalId)
                    return i;
            }
            return -1;
        }

        /*
         * Various slot index functions so we don't get the directions wrong
         */

        // Get the western-most slot index number
        private int WestSlotIndex()
        {
            return 0;  // array starts from the left -- hardcoded values could be modified with more refactoring.
        }

        // Get the eastern-most slot index number
        private int EastSlotIndex()
        {
            return (slots.Count() - 1);  // 1 less because array is 0-based
        }

        // get the middle slot index number
        private int MiddleSlotIndex()
        {
            return (slots.Count() / 2);  // returns 1 if there are 3 slots, works for the monkey rope,
        }

        /*
         * Determine if certain slots are empty
         */

        private bool EastSlotEmpty()
        {
            return (slots[EastSlotIndex()] == null);
        }
        private bool WestSlotEmpty()
        {
            return (slots[WestSlotIndex()] == null);
        }
        private bool MiddleSlotEmpty()
        {
            return (slots[MiddleSlotIndex()] == null);
        }



        // find out if the eastern or western most slot is open
        public bool IsRopeFirstSlotOpen(Locale locale)
        {
            if (locale == East) {
                return (EastSlotEmpty());
            } else if (locale == West) {
                return (WestSlotEmpty());
            } else {
                return false;
            }
        }

        // All checks were previously made, animal is moving to first slot next to cliff
        public void MoveToRope(Animal animal, Locale locale)
        {
            switch (locale) {
                case East:
                    slots[EastSlotIndex()] = animal;
                    break;
                case West: // location can only be east or west
                    slots[WestSlotIndex()] = animal;
                    break;
            }

            globalRepository.ReportProgress(CreateMemento());
        }


        // Moves always work.  So, you have to check if they will work before calling the code.

        public Direction MonkeysDirectionOnRope()
        {
            Direction direction = Direction.None;

            foreach (Animal animal in slots) {
                if (animal != null)
                    return animal.direction;
            }
            return direction;
        }

        public override int ProcessTempo()
        {
            if (Count() > 0) {  // There are animals on the rope
                TryToMoveOneAnimal();
            }
            globalRepository.ReportProgress(CreateMemento());
            return 0;
        }

        // Moves animals along the rope only
        private bool TryToMoveOneAnimal()
        {

            // it is better to pick up the monkey in the back, so for Westward ropes, 
            // we start looking for the first animal to move by checking right to left.
            if (MonkeysDirectionOnRope() == Direction.Westward) {
                for (int slotNumber = EastSlotIndex(); slotNumber >= 0; slotNumber--) {
                    if (slots[slotNumber] != null) {
                        if (TryToMove(slotNumber)) {
                            return true;
                        }
                    }
                }
                // check for a monkey left to right for eastward moving ropes
            } else {
                if (MonkeysDirectionOnRope() == Direction.Eastward) {
                    for (int slotNumber = 0; slotNumber < MonkeySlots; slotNumber++) {
                        if (slots[slotNumber] != null) {
                            if (TryToMove(slotNumber)) {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        private int NextEastSlot(int slot)
        {
            return slot + 1;
        }
        private int NextWestSlot(int slot)
        {
            return slot - 1;
        }

        private bool TryToMove(int slotNumber)
        {
            Animal animal = slots[slotNumber];
            if (slotNumber == EastSlotIndex() &&
                animal.direction == Direction.Eastward) {
                // this is the last slot on the right, we can remove the animal
                // as if it had just gotten off the bridge to the east
                RemoveAnimalFromRope(slotNumber);
                return true;
            }

            if (slotNumber == WestSlotIndex() &&
                animal.direction == Direction.Westward) {
                // this is the last slot on the left, we can remove the animal
                // as if it had just gotten off the bridge to the west
                RemoveAnimalFromRope(slotNumber);
                return true;
            }

            // moving eastward and next most eastern slot is open
            if (slotNumber < EastSlotIndex() &&
                animal.direction == Direction.Eastward &&
                slots[NextEastSlot(slotNumber)] == null) {
                // this animal is moving right, and there is an empty slot next to it,  
                // Go ahead and move it over one
                MoveAnimalEast(slotNumber);
                return true;
            }

            // moving westard and the next most western slot is open
            if (slotNumber > WestSlotIndex() &&
                animal.direction == Direction.Westward &&
                slots[NextWestSlot(slotNumber)] == null) {
                // this animal is moving right, and there is an empty slot next to it,  
                // Go ahead and move it over one
                MoveAnimalWest(slotNumber);
                return true;
            }
            // If we have gotten here, the animal is boxed in and cannot move
            return false;
        }

        private void MoveAnimalWest(int slotNumber)
        {
            slots[NextWestSlot(slotNumber)] = slots[slotNumber];
            slots[slotNumber] = null;
            globalRepository.ReportProgress(CreateMemento());
        }

        private void MoveAnimalEast(int slotNumber)
        {
            slots[NextEastSlot(slotNumber)] = slots[slotNumber];
            slots[slotNumber] = null;
            globalRepository.ReportProgress(CreateMemento());
        }

        private void RemoveAnimalFromRope(int slotNumber)
        {
            Animal animal = slots[slotNumber];  // create a temp variable to make code easier to read
            animalsProcessed++;
            if (animal.direction == Direction.Westward)
                animalsProcessedWestbound++;
            else if (animal.direction == Direction.Eastward)
                animalsProcessedEastbound++;
            animal.ClearRepository();
            slots[slotNumber] = null;

            globalRepository.ReportProgress(CreateMemento());
        }
    }
}
