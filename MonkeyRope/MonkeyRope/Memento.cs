﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonkeyRope
{
    // Use the memento class for the various objects to pass their state back up to the View
    public struct Memento
    {
        public Repository.Locale mLocale;  // whether this is the east cliff pool, the center (rope), or the west cliff pool
        public bool mGateOpen;   // for pools, whether its gate is open
        public int mCount;       // number of animals on the rope or in a pool -- in a given repository
        public List<SlotMomento> mSlots; // for the rope, the animalIds 
        public int mTurnCount;
        public int mWaitCount;   // current # of cycles the gate has been closed with an animal waiting
        public int mWaitCountMax; // the maximum that waitCount has ever reached
        public int mAnimalsProcessed;  // how many animals have been processed by this repository
        public int mAnimalsProcessedEastbound;
        public int mAnimalsProcessedWestbound;
    }
}
