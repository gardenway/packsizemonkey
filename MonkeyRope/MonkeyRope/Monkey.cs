﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonkeyRope
{

    // So, I made the Monkey class a base class of Animal so that I could have 
    // different types of animals down the road.
    // turns out that I don't have anything special here yet, but I could in the future.
    //
    // As an example, I have created a "mass" member for each animal which is used by
    // the system to determine the number of animals that the ropoe will support,
    // affecting the number of slots allowed on the rope.
    public class Monkey : Animal
    {
        private const int MonkeyMass = 10; // mass of howler monkey in kg
        public Monkey(Direction directionArg, Repository repositoryArg)
            : base(directionArg, repositoryArg, MonkeyMass)
        {
            
        }
    }
}
