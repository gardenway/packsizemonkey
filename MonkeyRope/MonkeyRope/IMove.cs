﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonkeyRope
{
    public interface IMove
    {
        bool TryMoveFromRepository(Animal animal);
        int ProcessTempo();
    }
}
