﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MonkeyRope
{
    using static Repository;
    using static Repository.RepositoryType;

    public class RepositoryFactory : ILog
    {
        public void Log() { }
        public Repository.Locale location;
        
        private static RepositoryFactory _instance = null; // singleton pointer for AnimalFactory

        // repository factory singleton
        public static RepositoryFactory GetInstance()
        {
            if (_instance == null) {
                _instance = new RepositoryFactory();
            }
            return _instance;
        }

        // Create a pool or a rope repository
        public Repository Create(RepositoryType respositoryType, Locale location,
            GlobalRepository globalRepository)
        {
            Repository repository = null;
            switch (respositoryType) {
                case RopeType:
                    repository = new Rope(globalRepository);
                    break;
                case PoolType:
                    repository = new Pool(location, globalRepository);
                    break;
            }
            return repository;
        }

    }
}

