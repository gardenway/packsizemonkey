﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;


namespace MonkeyRope
{
    public abstract class Animal : PackSizeObject, IProcessTempo
    {
        public enum AnimalType { Monkey, Leopard, Elephant, Lizard, Ant, Ghost };
        public enum Direction { Westward = 0, Eastward = 1, None };  // Direction this animal is headed
        public enum Position { InPool = 0, OnRope = 1, InNirvana = 2 };

        protected Repository repository;
        protected static int AnimalRestTime = 500;
        protected static int nextAnimalId = 1;
        public string name { get; protected set; }
        public Direction direction { get; protected set; }
        public int animalId { get; protected set; }
        public int mass { get; protected set; }// used to determine how many slots on rope
        
        public Animal(Direction directionArg, Repository repositoryArg, int massArg)
        {
            animalId = nextAnimalId++;   // keep track of this animals ID#
            direction = directionArg;    // which way does this monkey want to travel
            repository = repositoryArg;  // shows where the animal is (pool or rope)
            mass = massArg;              // initialize mass for derived classes
            name = NameGenerator.GetName();
        }

        public void Initialize(Direction directionArg, Repository repositoryArg)
        {
            direction = directionArg;
            repository = repositoryArg;
        }

        private Position DetermineCurrentPosition()
        {
            if (repository == null) {
                return Position.InNirvana;
            } else {
                switch (repository.locale) {
                    case Repository.Locale.Bridge:
                        return Position.OnRope;
                    case Repository.Locale.West:
                        return Position.InPool;
                    case Repository.Locale.East:
                        return Position.InPool;
                    default:
                        return Position.InNirvana; // removed from all repositories
                }
            }
        }

        public override int ProcessTempo()
        {
            // When an animal gets a chance to move, it follows this logic:
            //   1) Determine where the animal is (starting pool, on rope, or completed job)
            //   2) for
            //            InPool, determine
            //                  a) Is the pool's gate open?
            //                  b) is it at the front of the line?
            //                  c) Is the rope empty or flowing in our direction?
            //                  d) Is the first slot on the rope open?
            //                            THEN, this guy can move forward
            //
            //             OnRope, determine
            //                   a) Can we move to the next slot or off the rope?
            //                               THEN, this guy can move forward
            //
            //             InNirvana
            //                   then, do nothing.
            //          
            //Checks to see if the place it wants to move is
            //       a) Determine where the next place to move is (off the cliff onto the rope, over a slot, or off the rope
            //       b) Determine if we are allowed to move to that location
            //       c) Determine if the location is available
            //   2) If the later are true, it moves
            //
            //   Steps 1 and 2 must be atomic.  If we implement this as a thread, then we must put a lock
            //   around these two steps.
            //

            Position position = DetermineCurrentPosition();
            switch (position) {
                case Position.InPool:
                    if (CanLeavePool()) {
                        MoveToRope();
                    }
                    break;
                case Position.OnRope:
                    break;
                case Position.InNirvana:
                    break;
            }

            return 0;
        }

        // Only called by an Animal that is in a pool
        private bool CanLeavePool()
        {
            Pool pool = (Pool)repository;  // We are in a pool, so let's save a few casts
            if (!pool.IsAtFrontOfQueue(this))
                return false;

            if (pool.IsRopeBlocked())  // gate is closed or the direction is wrong
                return false;

            if (!pool.IsRopeFirstSlotOpen())
                return false;
            return true;
        }

        // We have already checked and we can move off the pool/cliff onto the rope
        private void MoveToRope()
        {
            ((Pool) repository).MoveToRope(this); // move onto the rope
            ((Pool) repository).Remove();          // take this animal out of the pool
        }

        public void ClearRepository()
        {
            repository = null;
        }

    }
}
