﻿namespace MonkeyRopeTest
{
    
    using static MonkeyRope.Repository.Locale;
    using static MonkeyRope.Repository.RepositoryType;
    using static MonkeyRope.Animal;

    using NUnit.Framework;
    using MonkeyRope;

    [TestFixture]
    class CreateMultipleAnimalTypesTest
    {
        static View view;
        [SetUp]
        public void Init()
        {
            view = new View();
            view.Show();
        }

        [TearDown]
        public void AfterTest()
        {
            view.Dispose();
            view = null;
        }
        [Test]
        public void InitTest()
        {
            view = new View();

            view.Show();

        }

        [Test]
        public void CreateLeopardTest()
        {
            GlobalRepository globalRepository = GlobalRepository.GetInstance(view);
            AnimalFactory animalFactory = AnimalFactory.GetInstance();
            Assert.NotNull(animalFactory);

            // run Get instance twice and confirm that 
            // we get the same factory (singleton is working)

            AnimalFactory animalFactory2 = AnimalFactory.GetInstance();
            Assert.AreEqual(animalFactory, animalFactory2);

            // create an eastern respository
            RepositoryFactory repositoryFactory = RepositoryFactory.GetInstance();
            Assert.NotNull(repositoryFactory);

            Repository eastPool = repositoryFactory.Create(PoolType, East, globalRepository);

            // create an animal of type monkey heading east
            Animal animal = animalFactory.
                Create(AnimalType.Leopard, Animal.Direction.Eastward, eastPool);

            var objectType = animal.GetType();
            Assert.AreEqual(objectType.FullName, "MonkeyRope.Leopard");
            Assert.AreEqual(animal.direction, Animal.Direction.Eastward);


        }

    }
}
