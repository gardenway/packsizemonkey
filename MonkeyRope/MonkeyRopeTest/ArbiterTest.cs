﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MonkeyRopeTest
{
    using NUnit.Framework;
    using MonkeyRope;
    using static MonkeyRope.Animal;

    [TestFixture]
    public class ArbiterTest
    {
        Arbiter arbiter;
        [SetUp]
        public void Init()
        {
            Form view = new View();
            arbiter = new Arbiter();
            view.Show();
        }

        [Test]
        public void ArbiterLogTest()
        {
            arbiter.Log();
            Assert.NotNull(arbiter);
        }

        [Test]
        public void ArbiterLogTest2()
        {
            arbiter.Log();
            Assert.AreEqual(1, 1);
        }
        [Test]
        public void ArbiterLogTest3()
        {
            arbiter.Log();
            Assert.AreEqual(1, 1);
        }
        [Test]
        public void CreateArbiterTest()
        {
            View view = new View();

            view.Show();

            Arbiter arbiter = Arbiter.GetInstance();
            Assert.NotNull(arbiter);
            GlobalRepository globalRepository = GlobalRepository.GetInstance(view);
            Assert.NotNull(globalRepository);

            Pool eastPool = globalRepository.GetEastPool();
            Assert.AreEqual(Repository.Locale.East, eastPool.locale);

            Pool westPool = globalRepository.GetWestPool();

            // Add West Monkeys
            for (int i = 0; i < 30; i++) {
                westPool.AddAnimal(AnimalType.Monkey);
                Assert.AreEqual(i + 1, westPool.Count());
            }
            // Add east monkeys Monkeys
            for (int i = 0; i < 40; i++) {
                eastPool.AddAnimal(AnimalType.Monkey);
                Assert.AreEqual(i + 1, eastPool.Count());
            }

            // Start Test -- by the end, we should see that both sides have had to wait.
            globalRepository.Start();


            // This does not work when the thread delay time is VERY short.
            //Assert.Greater(westPool.waitCountMax, 0);
            //Assert.Greater(eastPool.waitCountMax, 0);

        }
    }
}
