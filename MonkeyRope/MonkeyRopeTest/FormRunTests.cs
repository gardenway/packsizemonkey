﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MonkeyRopeTest
{
    public partial class FormRunTests : Form
    {
        public FormRunTests()
        {
            InitializeComponent();
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            ArbiterTest createTest = new ArbiterTest();

            createTest.Init();
            createTest.CreateArbiterTest();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CreateTest createTest = new CreateTest();

            createTest.Init();
            createTest.AddMonkeysToPool();
        }
    }
}
