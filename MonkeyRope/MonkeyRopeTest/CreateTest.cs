﻿

namespace MonkeyRopeTest
{
    using static MonkeyRope.Repository.Locale;
    using static MonkeyRope.Repository.RepositoryType;
    using static MonkeyRope.Animal;

    using NUnit.Framework;
    using MonkeyRope;

    [TestFixture]
    class CreateTest
    {

        [SetUp]
        public void Init()
        {
                    }

        [TearDown]
        public void AfterTest()
        {

        }
        [Test]
        public void InitTest()
        {
            View view = new View();

            view.Show();

        }

        [Test]
        public void CreateMonkeyTest()
        {
            View view = new View();

            view.Show();
            GlobalRepository globalRepository = GlobalRepository.GetInstance(view);
            AnimalFactory animalFactory = AnimalFactory.GetInstance();
            Assert.NotNull(animalFactory);

            // run Get instance twice and confirm that 
            // we get the same factory (singleton is working)

            AnimalFactory animalFactory2 = AnimalFactory.GetInstance();
            Assert.AreEqual(animalFactory, animalFactory2);

            // create an eastern respository
            RepositoryFactory repositoryFactory = RepositoryFactory.GetInstance();
            Assert.NotNull(repositoryFactory);

            Repository eastPool = repositoryFactory.Create(PoolType, East,
                globalRepository);

            // create an animal of type monkey heading east
            Animal animal = animalFactory.
                Create(AnimalType.Monkey, Animal.Direction.Eastward, eastPool);
                
            var objectType = animal.GetType();
            Assert.AreEqual(objectType.FullName, "MonkeyRope.Monkey");
            Assert.AreEqual(animal.direction, Animal.Direction.Eastward);
            view.Dispose();
            view = null;

        }

        [Test]
        public void CreatePoolTest()
        {
            View view = new View();

            view.Show();
            GlobalRepository globalRepository = GlobalRepository.GetInstance(view);
            RepositoryFactory repositoryFactory = RepositoryFactory.GetInstance();
            Assert.NotNull(repositoryFactory);

            // run Get instance twice and confirm that 
            // we get the same factory (singleton is working)

            RepositoryFactory repositoryFactory2 = RepositoryFactory.GetInstance();
            Assert.AreEqual(repositoryFactory, repositoryFactory2);

            // create an eastern respository
           Repository eastPool = repositoryFactory.
                Create(PoolType, Repository.Locale.East, globalRepository);
            // create a respository of type pool for east headng animals
            Repository westPool = repositoryFactory2.
                Create(PoolType, Repository.Locale.West, globalRepository);

            var objectType = eastPool.GetType();
            Assert.AreEqual(objectType.FullName, "MonkeyRope.Pool");
            Assert.AreEqual(Repository.Locale.East, eastPool.locale);
            Assert.AreEqual(Repository.Locale.West, westPool.locale);
            view.Dispose();
            view = null;
        }

        [Test]
        public void AddMonkeysToPool()
        {
            View view = new View();

            view.Show();
            GlobalRepository globalRepository = GlobalRepository.GetInstance(view);
            RepositoryFactory repositoryFactory = RepositoryFactory.GetInstance();
            Pool eastPool = (Pool)repositoryFactory.Create(PoolType, East, globalRepository);
       
            Assert.AreEqual(eastPool.Count(), 0);

            AnimalFactory animalFactory = AnimalFactory.GetInstance();
            for(int i = 0; i < 100; i++) {
                eastPool.AddAnimal(AnimalType.Monkey);
                Assert.AreEqual(eastPool.Count(), i+1);
            }


            Pool westPool = (Pool)repositoryFactory.Create(PoolType, East, globalRepository);
            Assert.AreEqual(westPool.Count(), 0);

            for (int i = 0; i < 100; i++) {
                westPool.AddAnimal(AnimalType.Monkey);
                Assert.AreEqual(westPool.Count(), i + 1);
            }

        }
    }
}
